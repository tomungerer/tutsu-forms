<?php

// STEPS :
// 1. Update admin
// 2. Send email to user
// 3. Verify email and update admin
// 4. Send email to admin

function tutsu_forms_appointment_submission(){

  // Fields
  if (isset($_POST["your-fname"])) {
    $your_fname = $_POST['your-fname'];
  } else {
    $your_fname = null;
  }

  if (isset($_POST["your-lname"])) {
    $your_lname = $_POST['your-lname'];
  } else {
    $your_lname = null;
  }
  if (isset($_POST["parent-fname"])) {
    $parent_fname = $_POST['parent-fname'];
  } else {
    $parent_fname = null;
  }

  if (isset($_POST["parent-lname"])) {
    $parent_lname = $_POST['parent-lname'];
  } else {
    $parent_lname = null;
  }

  if (isset($_POST["your-email"])) {
    $your_email = $_POST['your-email'];
  } else {
    $your_email = null;
  }

  if (isset($_POST["your-phone"])) {
    $your_phone = $_POST['your-phone'];
  } else {
    $your_phone = null;
  }

  if (isset($_POST["your-preferred-time-h"])) {
    $your_preferred_time_h = $_POST['your-preferred-time-h'];
  } else {
    $your_preferred_time_h = null;
  }

  if (isset($_POST["your-preferred-time-m"])) {
    $your_preferred_time_m = $_POST['your-preferred-time-m'];
  } else {
    $your_preferred_time_m = null;
  }

  if (isset($_POST["your-preferred-time-ampm"])) {
    $your_preferred_time_ampm = $_POST['your-preferred-time-ampm'];
  } else {
    $your_preferred_time_ampm = null;
  }

  if (isset($_POST["your-source"])) {
    $your_source = $_POST['your-source'];
  } else {
    $your_source = null;
  }

  if (isset($_POST["your-message"])) {
    $your_message = $_POST['your-message'];
  } else {
    $your_message = null;
  }

  // Page ID
  if (isset($_POST["the-page-id"])) {
    $page_id = $_POST['the-page-id'];
  } else {
    $page_id = null;
  }

  // Unique User Key
  $key = str_split('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._~');
  shuffle($key);
  $key = implode('', $key);

  $your_preferred_time = $your_preferred_time_h . ':' . $your_preferred_time_m . ' ' . $your_preferred_time_ampm;

  $your_submission = 'Name: ' . $your_fname . ' ' . $your_lname . '
Parent Name: ' . $your_parent_fname . ' ' . $your_parent_fname . '
Email: ' . $your_email . '
Phone: ' . $your_phone . '
Preferred Time: ' . $your_preferred_time . '
Source: ' . $your_source . '
Subject: ' . $your_subject . '
Message: ' . $your_message;

  // Create submission
  $submission = array(
    'submission' => $your_submission,
    'page_id' => $page_id,
    'key'     => $key,
  );

  // Update admin + send email to client on form submit
  if(isset($_POST['appointment_form_submit']) && $submission ){

    tutsu_forms_appointment_update($submission);
    tutsu_forms_appointment_send_email_to_user($submission);

  }

}
add_action( 'init', 'tutsu_forms_appointment_submission' );

// 1. UPDATE ADMIN
function tutsu_forms_appointment_update($submission){

  $row_id = add_row('field_tutsu_forms_submissions_appointment', $submission, 'options');

}

// 2. SEND EMAIL TO USER
function tutsu_forms_appointment_send_email_to_user($submission){

    // User info
    $user_fname   = $submission['fname'];
    $user_lname   = $submission['lname'];
    $user_name    = $submission['fname'] . ' ' . $submission['lname'];
    $user_email   = $submission['email'];
    $user_phone   = $submission['phone'];
    $user_page_id = $submission['page_id'];
    $user_key     = $submission['key'];

    // Verification link
    $link = esc_url( get_permalink($user_page_id) ) . '?key=' . $user_key;

    // Email image header
    $image = get_field('tutsu_forms_settings_appointment_header_image', 'options');

    // Email recipient
    $to = get_field('tutsu_forms_settings_appointment_email', 'options') . ',' . get_option( 'admin_email' );
    $sanitized_to = sanitize_email( $to );

    // Email subject
    $subject = get_field('tutsu_forms_settings_appointment_subject', 'options');
    if(!$subject){
      $subject = 'You have booked an appointment at ' . get_option('blogname');
    }

    // Email body
    $body = get_field('tutsu_forms_settings_appointment_body', 'options');
    if($body){
      $body = str_replace('{{name}}', $user_name, $body);
      $body = str_replace('{{fname}}', $user_fname, $body);
      $body = str_replace('{{lname}}', $user_lname, $body);
      $body = str_replace('{{email}}', $user_email, $body);
      $body = str_replace('{{phone}}', $user_phone, $body);
      $body = str_replace('{{link}}', $link, $body);
    }

    // Fetch email template
    $atts = array (
      'body' => $body,
      'link' => $link,
      'image' => $image,
      'title' => $subject,
      'name' => $user_name
    );
    $body = tutsu_forms_email_template_appointment($atts);

    // Email headers
    $headers = array('Content-Type: text/html; charset=UTF-8');

    // Send email
    if ( $sanitized_to != '' ) {
      wp_mail( $to, $subject, $body, $headers );
    }

}

// 3. VERIFY EMAIL AND UPDATE ADMIN
function tutsu_forms_appointment_verify_email_and_update_admin(){

  if (isset($_GET["key"])) {
    $tutsu_forms_offer_submission_key = $_GET['key'];
  } else {
    $tutsu_forms_offer_submission_key = null;
  }

  $key_submission = array(
    'key' => $tutsu_forms_offer_submission_key,
  );

  $key_submission_verified = array(
    'verified' => true,
  );

  $submissions = get_field('tutsu_forms_submissions_appointment', 'options');
  if( $submissions ) {
    $i = 0;
    foreach ($submissions as $submission) {
      $i++;
      $key = $submission['key'];
      $verified = $submission['verified'];
      if($key == $tutsu_forms_offer_submission_key){
        // update row
        update_row('tutsu_forms_submissions_appointment', $i, $key_submission_verified, 'options');
        // email admin
        tutsu_forms_appointment_send_email_to_admin($submission);
      }
    }
  }

}
add_action( 'init', 'tutsu_forms_appointment_verify_email_and_update_admin' );

// 4. SEND EMAIL TO ADMIN
  function tutsu_forms_appointment_send_email_to_admin($submission){

    // Email recipient
    $to = get_field('tutsu_forms_settings_appointment_email', 'options');
    if(!$to){
      $to = get_option( 'admin_email' );
    }
    $sanitized_to = sanitize_email( $to );

    // Email subject
    $subject = 'New Submission on ' . get_option('blogname');

    // Link
    $link = get_option( 'home' ) . '/wp-admin/admin.php?page=theme-settings-forms-appointment';

    // Fetch email template
    $atts = array (
      'link' => $link,
      'submission' => $submission
    );
    $body = tutsu_forms_email_template_appointment_admin($atts);

    // Email headers
    $headers = array('Content-Type: text/html; charset=UTF-8');

    // Send email
    if ( $sanitized_to != '' ) {
      wp_mail( $to, $subject, $body, $headers );
    }

}

// Shortcode
function tutsu_forms_appointment_shortcode($atts = ''){

  $recaptcha = get_field('tutsu_forms_settings_recaptcha_activate', 'options');
  $recaptcha_key = get_field('tutsu_forms_settings_recaptcha_site_key', 'options');

  $defaults = array(
    'redirect' => '',
    'btn_class' => 'btn btn-1'
  );

  $atts = wp_parse_args( $atts, $defaults );

  $redirect = $atts['redirect'];
  $btn_class = $atts['btn_class'];

  $output = '';

  $output .= '<div class="form-wrapper form-wrapper-appointment">';
  $output .= '<form id="form_appointment" method="POST"';
  if($redirect){
    $output .= 'action="' . $redirect . '"';
  }
  $output .= '>';
  if ($_SERVER['REQUEST_METHOD'] == 'POST'){
    if (!isset($_GET['key'])) {
      $output .= '<fieldset class="form-group note"><p>Thank you for your enquiry.<br>Please check your emails.</p></fieldset>';
    } else {
      $output .= '<fieldset class="form-group note"><p>Thank you, your email has been verified. We\'ll be in touch.</p></fieldset>';
    }
  } else {
    $output .= '<fieldset class="form-group fname">';
    $output .= '<label for="your-fname">First Name <span>*</span></label>';
    $output .= '<input type="text" name="your-fname" class="form-control" placeholder="Your first name..." required>';
    $output .= '</fieldset>';
    $output .= '<fieldset class="form-group lname">';
    $output .= '<label for="your-lname">Last Name <span>*</span></label>';
    $output .= '<input type="text" name="your-lname" class="form-control" placeholder="Your last name..." required>';
    $output .= '</fieldset>';
    $output .= '<fieldset class="form-group fname">';
    $output .= '<label for="parent-fname">Parent First Name</label>';
    $output .= '<input type="text" name="parent-fname" class="form-control" placeholder="Parent first name...">';
    $output .= '<small>(If patient is under 18 years of age.)</small>';
    $output .= '</fieldset>';
    $output .= '<fieldset class="form-group lname">';
    $output .= '<label for="parent-lname">Parent Last Name</label>';
    $output .= '<input type="text" name="parent-lname" class="form-control" placeholder="Parent last name...">';
    $output .= '</fieldset>';
    $output .= '<fieldset class="form-group email">';
    $output .= '<label for="your-email">Email Address <span>*</span></label>';
    $output .= '<input type="email" name="your-email" class="form-control" placeholder="Your email address..." required>';
    $output .= '</fieldset>';
    $output .= '<fieldset class="form-group phone">';
    $output .= '<label for="your-phone">Phone <span>*</span></label>';
    $output .= '<input type="tel" name="your-phone" class="form-control" placeholder="Your phone number..." required>';
    $output .= '</fieldset>';
    $output .= '<fieldset class="form-group time">';
    $output .= '<label for="your-preferred-time">Preferred Time <span>*</span></label>';
    $output .= '<div style="max-width: 200px">';
    $output .= '<select class="form-control" name="your-preferred-time-h" style="width:33%; display:inline-block"><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option><option value="8">8</option><option value="9">9</option><option value="10">10</option><option value="11">11</option><option value="12">12</option></select>';
    $output .= '<select class="form-control" name="your-preferred-time-m" style="width:33%; display:inline-block"><option value="00">00</option><option value="15">15</option><option value="30">30</option><option value="45">45</option></select>';
    $output .= '<select class="form-control" name="your-preferred-time-ampm" style="width:33%; display:inline-block"><option value="AM">AM</option><option value="PM">PM</option></select>';
    $output .= '</div>';
    $output .= '</fieldset>';
    $output .= '<fieldset class="form-group source">';
    $output .= '<label for="your-source">How did you hear about the practice? <span>*</span></label>';
    $output .= '<select class="form-control" name="your-source"><option value="Google">Google</option><option value="Print Ad">Print Ad</option><option value="Billboard">Billboard</option><option value="Referral">Referral</option></select>';
    $output .= '</fieldset>';
    $output .= '<fieldset class="form-group message">';
    $output .= '<label for="your-message">Tell us about your dental needs: <span>*</span></label>';
    $output .= '<textarea type="textarea" name="your-message" rows="6" class="form-control" placeholder="Your message..." required></textarea>';
    $output .= '</fieldset>';
    if($recaptcha){
      $output .= '<fieldset class="form-group">';
      $output .= '<div class="g-recaptcha" data-sitekey="' . $recaptcha_key . '"></div>';
      $output .= '</fieldset>';
    }
    $output .= '<div style="visibility: hidden; height: 0; width: 0; overflow: hidden;"><input type="text" name="the-page-id" value="' . get_the_ID() .'"></div>';
    $output .= '<div class="form-group"><input type="submit" name="appointment_form_submit" class="' . $btn_class . '" value="Submit"><br><small>(<span>*</span> required fields)</small></div>';
  }
  $output .= '</form>';
  $output .= '</div>';

  return $output;
}
add_shortcode('form_appointment', 'tutsu_forms_appointment_shortcode');