<?php

// STEPS :
// 1. Update admin
// 2. Send email to user
// 3. Verify email and update admin
// 4. Send email to admin

add_action( 'init', 'tutsu_forms_contact_submission' );
function tutsu_forms_contact_submission(){

  // Fields
  if (isset($_POST["your-fname"])) {
    $your_fname = $_POST['your-fname'];
  } else {
    $your_fname = null;
  }

  if (isset($_POST["your-lname"])) {
    $your_lname = $_POST['your-lname'];
  } else {
    $your_lname = null;
  }

  if (isset($_POST["your-email"])) {
    $your_email = $_POST['your-email'];
  } else {
    $your_email = null;
  }

  if (isset($_POST["your-phone"])) {
    $your_phone = $_POST['your-phone'];
  } else {
    $your_phone = null;
  }

  if (isset($_POST["your-subject"])) {
    $your_subject = $_POST['your-subject'];
  } else {
    $your_subject = null;
  }

  if (isset($_POST["your-message"])) {
    $your_message = $_POST['your-message'];
  } else {
    $your_message = null;
  }

  // Page ID
  if (isset($_POST["the-page-id"])) {
    $page_id = $_POST['the-page-id'];
  } else {
    $page_id = null;
  }

  // Unique User Key
  $key = str_split('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._~');
  shuffle($key);
  $key = implode('', $key);

  // Date
  $date = date("d/m/Y H:i:s");

  $your_submission = 'Submission received: ' . $date . '<br>
<br>
Name: ' . $your_fname . ' ' . $your_lname . '<br>
Email: ' . $your_email . '<br>
Phone: ' . $your_phone . '<br>
Subject: ' . $your_subject . '<br>
Message: ' . $your_message;

  // Create submission
  $submission = array(
    'fname'   => $your_fname,
    'lname'   => $your_lname,
    'email'   => $your_email,
    'phone'   => $your_phone,
    'subject'   => $your_subject,
    'message'   => $your_message,
    'page_id' => $page_id,
    'key'     => $key,
    'submission'   => $your_submission,
  );

  // Update admin + send email to client on form submit
  if(isset($_POST['contact_form_submit']) && $submission ){
    tutsu_forms_contact_update($submission);
    tutsu_forms_contact_send_email_to_user($submission);
  }

}

// 1. UPDATE ADMIN
function tutsu_forms_contact_update($submission){

  $row_id = add_row('field_tutsu_forms_submissions_contact', $submission, 'options');

}

// 2. SEND EMAIL TO USER
function tutsu_forms_contact_send_email_to_user($submission){

  // User info
  $user_fname   = $submission['fname'];
  $user_lname   = $submission['lname'];
  $user_name    = $submission['fname'] . ' ' . $submission['lname'];
  $user_email   = $submission['email'];
  $user_phone   = $submission['phone'];
  $user_page_id = $submission['page_id'];
  $user_key     = $submission['key'];

  // Verification link
  $link = esc_url( get_permalink($user_page_id) ) . '?key=' . $user_key . '#form_contact';

  // Email image header
  $image = get_field('tutsu_forms_settings_contact_header_image', 'options');

  // Email recipient
  $to = $user_email;
  $sanitized_to = sanitize_email( $to );

  // Email subject
  $subject = get_field('tutsu_forms_settings_contact_subject', 'options');
  if(!$subject){
    $subject = 'Enquiry for ' . get_option('blogname');
  }

  // Email body
  $body = get_field('tutsu_forms_settings_contact_body', 'options');
  if($body){
    $body = str_replace('{{name}}', $user_name, $body);
    $body = str_replace('{{fname}}', $user_fname, $body);
    $body = str_replace('{{lname}}', $user_lname, $body);
    $body = str_replace('{{email}}', $user_email, $body);
    $body = str_replace('{{phone}}', $user_phone, $body);
    $body = str_replace('{{link}}', $link, $body);
  }

  // Fetch email template
  $atts = array (
    'body' => $body,
    'link' => $link,
    'image' => $image,
    'title' => $subject,
    'name' => $user_name
  );
  $body = tutsu_forms_email_template_contact($atts);

  // Email headers
  $headers = array('Content-Type: text/html; charset=UTF-8');

  // Send email
  if ( $sanitized_to != '' ) {
    wp_mail( $to, $subject, $body, $headers );
  }

}

// 3. VERIFY EMAIL AND UPDATE ADMIN
add_action( 'init', 'tutsu_forms_contact_verify_email_and_update_admin' );
function tutsu_forms_contact_verify_email_and_update_admin(){

  if (isset($_GET["key"])) {
    $tutsu_forms_contact_submission_key = $_GET['key'];
  } else {
    $tutsu_forms_contact_submission_key = null;
  }

  $key_submission = array(
    'key' => $tutsu_forms_contact_submission_key,
  );

  $key_submission_verified = array(
    'verified' => true,
  );

  $submissions = get_field('tutsu_forms_submissions_contact', 'options');
  if( $submissions ) {
    $i = 0;
    foreach ($submissions as $submission) {
      $i++;
      $key = $submission['key'];
      $verified = $submission['verified'];
      if($key == $tutsu_forms_contact_submission_key){
        // update row
        update_row('tutsu_forms_submissions_contact', $i, $key_submission_verified, 'options');
        // email admin
        tutsu_forms_contact_send_email_to_admin($submission);
      }
    }
  }

}

// 4. SEND EMAIL TO ADMIN
function tutsu_forms_contact_send_email_to_admin($submission){

  // Email recipient
  $to = get_field('tutsu_forms_settings_contact_email', 'options');
  if(!$to){
    $to = get_option( 'admin_email' );
  }
  $sanitized_to = sanitize_email( $to );

  // Email subject
  $subject = 'New Submission on ' . get_option('blogname');

  // Link
  $link = get_option( 'home' ) . '/wp-admin/admin.php?page=theme-settings-forms-contact';

  // Fetch email template
  $atts = array (
    'link' => $link,
    'submission' => $submission
  );
  $body = tutsu_forms_email_template_contact_admin($atts);

  // Email headers
  $headers = array('Content-Type: text/html; charset=UTF-8');

  // Send email
  if ( $sanitized_to != '' ) {
    wp_mail( $to, $subject, $body, $headers );
  }

}

// Shortcode
add_shortcode('form_contact', 'tutsu_forms_contact_shortcode');
function tutsu_forms_contact_shortcode($atts = ''){

  $defaults = array(
    'redirect' => '',
    'btn_class' => 'btn btn-1'
  );

  $atts = wp_parse_args( $atts, $defaults );

  $redirect = $atts['redirect'];
  $btn_class = $atts['btn_class'];

  $output = '';

  $output .= '<div class="form-wrapper form-wrapper-contact">';
  $output .= '<form id="form_contact" method="POST"';
  if($redirect){
    $output .= 'action="' . $redirect . '"';
  }
  $output .= '>';
  if (isset($_GET['key'])) {
    $output .= '<fieldset class="form-group note"><p>Thank you, your email has been verified. I\'ll be in touch.</p></fieldset>';
  } else {
    if ($_SERVER['REQUEST_METHOD'] == 'POST'){
      $output .= '<fieldset class="form-group note"><p>Thank you for your submission.<br>Please check your emails.</p></fieldset>';
    } else {
      $output .= '<fieldset class="form-group fname">';
      $output .= '<label for="your-fname">First Name <span>*</span></label>';
      $output .= '<input type="text" name="your-fname" class="form-control" placeholder="Your first name..." required>';
      $output .= '</fieldset>';
      $output .= '<fieldset class="form-group lname">';
      $output .= '<label for="your-lname">Last Name</label>';
      $output .= '<input type="text" name="your-lname" class="form-control" placeholder="Your last name...">';
      $output .= '</fieldset>';
      $output .= '<fieldset class="form-group email">';
      $output .= '<label for="your-email">Email Address <span>*</span></label>';
      $output .= '<input type="email" name="your-email" class="form-control" placeholder="Your email address..." required>';
      $output .= '</fieldset>';
      $output .= '<fieldset class="form-group phone">';
      $output .= '<label for="your-phone">Phone</label>';
      $output .= '<input type="tel" name="your-phone" class="form-control" placeholder="Your phone number...">';
      $output .= '</fieldset>';
      $output .= '<fieldset class="form-group subject">';
      $output .= '<label for="your-subject">Subject</label>';
      $output .= '<input type="text" name="your-subject" class="form-control" placeholder="Your subject...">';
      $output .= '</fieldset>';
      $output .= '<fieldset class="form-group message">';
      $output .= '<label for="your-message">Message</label>';
      $output .= '<textarea type="textarea" name="your-message" rows="6" class="form-control" placeholder="Your message..."></textarea>';
      $output .= '</fieldset>';
      $output .= '<div style="visibility: hidden; height: 0; width: 0; overflow: hidden;"><input type="text" name="the-page-id" value="' . get_the_ID() .'"></div>';
      $output .= '<div class="form-group"><input type="submit" name="contact_form_submit" class="' . $btn_class . '" value="Submit"><br><small>(<span>*</span> required fields)</small></div>';
    }
  }
  $output .= '</form>';
  $output .= '</div>';

  return $output;
}