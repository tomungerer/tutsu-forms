<?php

$forms=glob( TUTSU_FORMS_PLUGIN_DIR . '/public/forms/*.php');
foreach( $forms as $form ) {
  include( $form );
}

$email_templates=glob( TUTSU_FORMS_PLUGIN_DIR . '/public/email_templates/*.php');
foreach( $email_templates as $email_template ) {
  include( $email_template );
}