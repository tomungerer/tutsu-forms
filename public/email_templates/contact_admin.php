<?php

function tutsu_forms_email_template_contact_admin($atts) {

  $defaults = array(
    'link'   => '#',
    'submission' => '',
  );

  $atts = wp_parse_args( $atts, $defaults );

  $link = $atts['link'];
  $submission = $atts['submission'];

  // $color_1 = get_field('color_1', 'option');
  // if(!$color_1){
  $color_1 = '#222';
  // }

  $title = 'New Submission at<br />' . get_option('blogname');

  $body = '

      <table width="640" cellspacing="0" cellpadding="0" border="0" align="center" style="max-width:640px; width:100%;" bgcolor="#FFFFFF">
        <tr>
          <td align="center" valign="top" style="padding:10px;">

            <table width="600" cellspacing="0" cellpadding="0" border="0" align="center" style="max-width:600px; width:100%;">
              <tr>
                <td align="center" valign="top" style="padding:10px;">
                  <h2>New Submission at<br /><a href="' . get_option( 'home' ) . '" target="_blank">' . get_option('blogname') . '</a></h2>
                  You have received a new submission with a verified email.
                </td>
              </tr>
            </table>

          </td>
        </tr>
      </table>

      <table width="640" cellspacing="0" cellpadding="0" border="0" align="center" style="max-width:640px; width:100%;" bgcolor="#FFFFFF">
        <tr>
          <td align="center" valign="top" style="padding:10px;">

            <table width="200" height="44" cellpadding="0" cellspacing="0" border="0" bgcolor="' . $color_1 . '" style="border-radius:4px;">
              <tr>
                <td align="center" valign="middle" height="44" style="font-family: Arial, sans-serif; font-size:14px; font-weight:bold;">
                  <a href="' . $link . '" target="_blank" style="font-family: Arial, sans-serif; color:#ffffff; display: inline-block; text-decoration: none; line-height:44px; width:200px; font-weight:bold;">View Submissions</a>
                </td>
              </tr>
            </table>

          </td>
        </tr>
        <tr>
          <td height="30" style="font-size:30px; line-height:30px;">&nbsp;</td>
        </tr>
      </table>

      <table width="640" cellspacing="0" cellpadding="0" border="0" align="center" style="max-width:640px; width:100%;" bgcolor="#FFFFFF">
        <tr>
          <td align="center" valign="top" style="padding:10px;">

            <table width="640" cellspacing="0" cellpadding="0" border="0" align="center" style="max-width:640px; width:100%;" bgcolor="#EEEEEE">
              <tr>
                <td align="left" valign="top" style="padding:10px;">

                  Source: <a href="' . get_permalink($submission['page_id']) . '" target="_blank">' . get_option('blogname') . ' - ' . get_the_title($submission['page_id']) . '</a><br>
                  ' . $submission['submission'] . '

                </td>
              </tr>
            </table>

          </td>
        </tr>
      </table>

';

  $atts = array(
    'title' => $title,
    'body'  => $body
  );

  return tutsu_forms_email_template_default($atts);

}