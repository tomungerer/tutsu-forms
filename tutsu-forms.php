<?php
/*
Plugin Name: TUTSU Forms
Plugin URI: http://fungtutsu.co.uk/
Description: TUTSU add-on.
Author: Tom Ungerer
Author URI: http://tomungerer.com/
Text Domain: tutsu-forms
Version: 1.0.1
Bitbucket Plugin URI: tomungerer/tutsu-forms
Bitbucket Plugin URI: https://bitbucket.org/tomungerer/tutsu-forms
*/

define( 'TUTSU_FORMS_VERSION', '1.0.1' );

define( 'TUTSU_FORMS_REQUIRED_WP_VERSION', '4.7' );

define( 'TUTSU_FORMS_PLUGIN', __FILE__ );

define( 'TUTSU_FORMS_PLUGIN_DIR', untrailingslashit( dirname( TUTSU_FORMS_PLUGIN ) ) );

// ACF
require_once ABSPATH . '/wp-content/plugins/advanced-custom-fields-pro/acf.php';

// TUTSU
// require_once ABSPATH . '/wp-content/plugins/tutsu/tutsu.php';

// ADMIN
require_once TUTSU_FORMS_PLUGIN_DIR . '/admin/admin.php';

// PUBLIC
require_once TUTSU_FORMS_PLUGIN_DIR . '/public/public.php';

// // CHECK FOR ERRORS

// add_action('activated_plugin','tutsu_save_error');
// function tutsu_save_error(){
//     update_option('plugin_error',  ob_get_contents());
// }

// add_action('deactivated_plugin','tutsu_unsave_error');
// function tutsu_unsave_error(){
//     update_option('plugin_error',  '');
// }

// echo get_option('plugin_error');