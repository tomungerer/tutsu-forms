<?php

$activates = get_field('tutsu_forms_settings_activate_forms', 'options');

if($activates){

  // APPOINTMENT SUBMISSIONS
  if(in_array('appointment', $activates)){
    acf_add_local_field_group(
      array (
        'key' => 'group_tutsu_forms_submissions_appointment',
        'title' => 'Submissions',
        'fields' => array (
          array (
            'key' => 'field_tutsu_forms_submissions_appointment',
            'label' => '',
            'name' => 'tutsu_forms_submissions_appointment',
            'type' => 'repeater',
            'button_label' => 'Add Submission',
            'layout' => 'block',
            'sub_fields' => array (
              array (
                'key' => 'field_tutsu_forms_submissions_appointment_submission',
                'label' => 'Submission',
                'name' => 'submission',
                'type' => 'textarea',
                'wrapper' => array (
                  'width' => '80',
                ),
              ),
              array (
                'key' => 'field_tutsu_forms_submissions_appointment_verified',
                'label' => 'Verified',
                'name' => 'verified',
                'type' => 'true_false',
                'wrapper' => array (
                  'width' => '20',
                ),
              ),
              array (
                'key' => 'field_tutsu_forms_submissions_appointment_key',
                'label' => '',
                'name' => 'key',
                'class' => 'hidden',
                'type' => 'password',
                'wrapper' => array (
                  'width' => '0',
                ),
              ),
              array (
                'key' => 'field_tutsu_forms_submissions_appointment_page_id',
                'label' => '',
                'name' => 'page_id',
                'class' => 'hidden',
                'type' => 'text',
                'wrapper' => array (
                  'width' => '0',
                ),
              ),
            ),
          ),
        ),
        'location' => array (
          array (
            array (
              'param' => 'post_type',
              'operator' => '==',
              'value' => 'form-submission',
            ),
            array (
              'param' => 'post_taxonomy',
              'operator' => '==',
              'value' => 'form-submission-category:appointment',
            ),
          ),
          array (
            array (
              'param' => 'options_page',
              'operator' => '==',
              'value' => 'theme-settings-forms-appointment',
            ),
          ),
        ),
        'menu_order' => 0,
        'position' => 'normal',
        'style' => 'seamless',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'active' => 1,
        'hide_on_screen' => array (
          0 => 'field_subtitle',
        ),
      )
    );
  }

}