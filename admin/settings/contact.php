<?php

$activates = get_field('tutsu_forms_settings_activate_forms', 'options');

if($activates){

  // CONTACT SETTINGS
  if(in_array('contact', $activates)){
    acf_add_local_field_group(
      array (
        'key' => 'group_tutsu_forms_settings_contact',
        'title' => 'Contact Form Settings <br><small>[form_contact]</small>',
        'fields' => array (
          array (
            'key' => 'field_tutsu_forms_settings_contact_email',
            'label' => 'Recipient Email',
            'name' => 'tutsu_forms_settings_contact_email',
            'type' => 'email',
          ),
          array (
            'key' => 'field_tutsu_forms_settings_contact_subject',
            'label' => 'Email Subject',
            'name' => 'tutsu_forms_settings_contact_subject',
            'type' => 'text',
          ),
          array (
            'key' => 'field_tutsu_forms_settings_contact_header_image',
            'label' => 'Header Image',
            'name' => 'tutsu_forms_settings_contact_header_image',
            'type' => 'image',
          ),
          array (
            'key' => 'field_tutsu_forms_settings_contact_body',
            'label' => 'Email Body',
            'name' => 'tutsu_forms_settings_contact_body',
            'type' => 'wysiwyg',
            'instructions' => 'Use {{name}} for Full Name, {{fname}} for First Name, {{lname}} for Last Name, {{email} for Email, {{phone}} for Phone and {{link}} for Verify Link.</a>',
          ),
        ),
        'location' => array (
          array (
            array (
              'param' => 'options_page',
              'operator' => '==',
              'value' => 'theme-settings-forms-settings',
            ),
          ),
        ),
        'menu_order' => 1,
        'position' => 'normal',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'active' => 1,
      )
    );
  }

}