<?php

$activates = get_field('tutsu_forms_settings_activate_forms', 'options');

if($activates){

  // APPOINTMENT SETTINGS
  if(in_array('appointment', $activates)){
    acf_add_local_field_group(
      array (
        'key' => 'group_tutsu_forms_settings_appointment',
        'title' => 'Appointment Form Settings <br><small>[form_appointment]</small>',
        'fields' => array (
          array (
            'key' => 'field_tutsu_forms_settings_appointment_email',
            'label' => 'Recipient Email',
            'name' => 'tutsu_forms_settings_appointment_email',
            'type' => 'email',
          ),
          array (
            'key' => 'field_tutsu_forms_settings_appointment_subject',
            'label' => 'Email Subject',
            'name' => 'tutsu_forms_settings_appointment_subject',
            'type' => 'text',
          ),
          array (
            'key' => 'field_tutsu_forms_settings_appointment_header_image',
            'label' => 'Header Image',
            'name' => 'tutsu_forms_settings_appointment_header_image',
            'type' => 'image',
          ),
          array (
            'key' => 'field_tutsu_forms_settings_appointment_body',
            'label' => 'Email Body',
            'name' => 'tutsu_forms_settings_appointment_body',
            'type' => 'wysiwyg',
            'instructions' => 'Use {{name}} for Name and {{email} for Email.<br> Advised to use : <a href="http://emailframe.work/" target="_blank">http://emailframe.work/</a>',
          ),
        ),
        'location' => array (
          array (
            array (
              'param' => 'options_page',
              'operator' => '==',
              'value' => 'theme-settings-forms-settings',
            ),
          ),
        ),
        'menu_order' => 2,
        'position' => 'normal',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'active' => 1,
      )
    );
  }

}