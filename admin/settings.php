<?php

if( function_exists('acf_add_local_field_group') ){

  // SETTINGS
  acf_add_local_field_group(
    array (
      'key' => 'group_tutsu_forms_settings',
      'title' => 'Settings',
      'fields' => array (
        array (
          'key' => 'field_tutsu_forms_settings_activate_forms',
          'label' => 'Activate Forms',
          'name' => 'tutsu_forms_settings_activate_forms',
          'type' => 'checkbox',
          'choices' => array(
            'contact' => 'Contact',
            'appointment' => 'Appointment',
          ),
          'layout' => 'horizontal',
        ),
      ),
      'location' => array (
        array (
          array (
            'param' => 'options_page',
            'operator' => '==',
            'value' => 'theme-settings-forms-settings',
          ),
        ),
      ),
      'menu_order' => 0,
      'position' => 'normal',
      'style' => 'default',
      'label_placement' => 'left',
      'instruction_placement' => 'label',
      'active' => 1,
    )
  );

  $settings=glob( TUTSU_FORMS_PLUGIN_DIR . '/admin/settings/*.php');
  foreach( $settings as $setting ) {
    include( $setting );
  }

}