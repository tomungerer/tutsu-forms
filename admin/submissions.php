<?php

if( function_exists('acf_add_local_field_group') ){

  $submissions=glob( TUTSU_FORMS_PLUGIN_DIR . '/admin/submissions/*.php');
  foreach( $submissions as $submission ) {
    include( $submission );
  }

}