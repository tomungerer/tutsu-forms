<?php

if( function_exists('acf_add_options_page') ) {
  acf_add_options_page(
    array(
      'page_title'  => 'Forms',
      'menu_title'  => 'TUTSU Forms',
      'menu_slug'   => 'theme-settings-forms',
      'icon_url'    => 'dashicons-list-view',
      'capability'  => 'edit_posts',
      'redirect'    => true
    )
  );

  acf_add_options_sub_page(
    array(
      'page_title'  => 'TUTSU Forms',
      'menu_title'  => 'Settings',
      'parent_slug' => 'theme-settings-forms',
      'menu_slug'   => 'theme-settings-forms-settings',
      'capability'  => 'edit_posts',
    )
  );

  $activates = get_field('tutsu_forms_settings_activate_forms', 'options');

  if($activates){

    if(in_array('contact', $activates)){
      acf_add_options_sub_page(
        array(
          'page_title'  => 'Contact Submissions',
          'menu_title'  => 'Contact',
          'parent_slug' => 'theme-settings-forms',
          'menu_slug'   => 'theme-settings-forms-contact',
          'capability'  => 'edit_posts',
        )
      );
    }
    if(in_array('appointment', $activates)){
      acf_add_options_sub_page(
        array(
          'page_title'  => 'Appointment Submissions',
          'menu_title'  => 'Appointment',
          'parent_slug' => 'theme-settings-forms',
          'menu_slug'   => 'theme-settings-forms-appointment',
          'capability'  => 'edit_posts',
        )
      );
    }
  }
}